/*
* add relations
*
* @ current user: the current user
*
* @ found user: the user the current user wants to start following
*
* add current user to the found user it followed followers list
*
*/
Parse.Cloud.define("addRelations", function(request, response) {

  Parse.Cloud.useMasterKey();

  var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", request.params.id);
  query.first({
    success: function(user) {

      // add current user to found user's followers relation
      var foundUser = user;
      var foundUserRelation = foundUser.relation("followers");
      foundUserRelation.add(Parse.User.current());
      Parse.Cloud.useMasterKey();
      foundUser.save();

      response.success(user);

    }
  })

});

Parse.Cloud.define("removeRelations", function(request, response) {

  Parse.Cloud.useMasterKey();

  var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", request.params.id);
  query.first({
    success: function(user) {

      // remove current user to found user's followers relation
      var foundUser = user;
      var foundUserRelation = foundUser.relation("followers");
      foundUserRelation.remove(Parse.User.current());
      Parse.Cloud.useMasterKey();
      foundUser.save();

      response.success(user);

    }
  })

});
