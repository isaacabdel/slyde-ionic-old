angular.module('slyde', ['ionic',
  'ngCordova',
  'slyde.services',
  'slyde.home',
  'slyde.welcome',
  'slyde.friends',
  'slyde.profile',
  'slyde.camera',
  'slyde.draw'
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('home', {
      url: '/home',
      controller: 'home',
      templateUrl: 'app/home/home.html'
    })
    .state('welcome', {
      url: '/welcome',
      controller: 'welcome',
      templateUrl: 'app/welcome/welcome.html'
    })

  $urlRouterProvider.otherwise('/welcome');
})
