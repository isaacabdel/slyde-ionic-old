angular.module('slyde.services', ['ionic'])

.factory('Auth', function($q) {

  return {

    signUp: function(u) {
      var deferred = $q.defer();
      var user = new Parse.User();
      user.set("username", u.username.toLowerCase());
      user.set("password", u.password.toLowerCase());
      user.set("email", u.email.toLowerCase());

      user.signUp(null, {
        success: function(user) {
          deferred.resolve(user)
        },
        error: function(user, error) {
          deferred.resolve(error)
        }
      });

      return deferred.promise;
    }

  }


})

.factory('Friends', function($q) {

  return {
    /**
     * gets all the users the current user is following
     * function is called in www/app/home/home.js
     * @return {Array} following: contains array of users the current user follows
     */
    getFollowing: function() {
      var deferred = $q.defer();
      var user = Parse.User.current();
      var relation = user.relation("following");
      relation.query().find({
        success: function(following) {
          deferred.resolve(following);
        },
        error: function(error) {}
      });
      return deferred.promise;
    },
    
    getFollowers: function() {
      var deferred = $q.defer();
      var user = Parse.User.current();
      var relation = user.relation("followers");
      relation.query().find({
        success: function(followers) {
          deferred.resolve(followers);
        },
        error: function(error) {}
      });
      return deferred.promise;
    }

  }


})


.factory('Slydes', function($q) {

  return {

    getSlydes: function(user) {
      var deferred = $q.defer();

      var Slydes = Parse.Object.extend("Slydes");
      var query = new Parse.Query(Slydes);
      query.equalTo("slydeTo", user.get('username'));
      query.find({
        success: function(Slydes) {
          deferred.resolve(Slydes)
        }
      });
      return deferred.promise;
    }

  }


})
