angular.module('slyde.camera', ['ionic'])

.controller('camera', function($scope, $ionicModal, $cordovaCamera) {

  // friends modal
  $ionicModal.fromTemplateUrl('app/camera/camera.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.cameraModal = modal;
  });
  $scope.openCameraModal = function() {
    $scope.cameraModal.show();
  };
  $scope.closeCameraModal = function() {
    $scope.cameraModal.hide();
  };

  /**
  * opens the camera and allows the user to take a picture
  * @return {}
  */
  $scope.takePicture = function() {

    var options = {
      quality: 100,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 450,
      targetHeight: 600,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.image = "data:image/jpeg;base64," + imageData;
    }, function(err) {
      // error
    });

    /**
    * posts the picture taken
    */
    $scope.uploadImage = function() {
      var image = $scope.image; // image
      var user = Parse.User.current(); // current user
      var date = new Date(); // date posted
      // parse image file
      var imageFile = new Parse.File("image.jpg", {
        base64: image
      });

      user.set("date", date); // date posted
      user.set("post", imageFile);
      user.save(null, {
        success: function() {
          alert('Yes')
        }
      });
    }


  }



})
