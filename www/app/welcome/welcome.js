angular.module('slyde.welcome', ['ionic'])

.controller('welcome', function($scope, $ionicModal, $ionicLoading, $cordovaDialogs, $state, Auth) {

  // log in modal
  $ionicModal.fromTemplateUrl('app/welcome/login.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.LogInModal = modal;
  });
  $scope.openLogInModal = function() {
    $scope.LogInModal.show();
    $scope.clearUserFormInputs(); // clear form
  };
  $scope.closeLogInModal = function() {
    $scope.LogInModal.hide();
  };

  // sign up modal
  $ionicModal.fromTemplateUrl('app/welcome/signup.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.SignUpModal = modal;
  });
  $scope.openSignUpModal = function() {
    $scope.SignUpModal.show();
    $scope.clearUserFormInputs(); // clear form
  };
  $scope.closeSignUpModal = function() {
    $scope.SignUpModal.hide();
  };

  // clear user form inputs
  $scope.clearUserFormInputs = function() {
    $scope.user = {
      username: null,
      password: null,
      email: null,
    };
  }

  // log In
  $scope.logIn = function() {
    // show loading overlay
    $ionicLoading.show({
      template: '<ion-spinner class="spinner-balanced" icon="android"></ion-spinner>'
    });

    var user = $scope.user;

    Parse.User.logIn(user.username.toLowerCase(), user.password.toLowerCase(), {
      success: function(user) {
        $scope.closeLogInModal(); // close log in modal
        $state.go('home', { // change to home state
          clear: true
        });
        $ionicLoading.hide(); // hide loading
      },
      error: function(user, error) {
        console.log(error);
        // check for log in errors
        if (error == 101) {
          var errormessage = "Invalid username/password"
          $scope.showLogInError(errormessage);
        } else {
          var errormessage = "An unknown error has occured"
          $scope.showLogInError(errormessage);
        }
      }
    });

  }

  // sign Up
  $scope.signUp = function() {
    // show loading overlay
    $ionicLoading.show({
      template: '<ion-spinner class="spinner-balanced" icon="android"></ion-spinner>'
    });

    var user = $scope.user;

    // tests
    var passwordPattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?_]/); // only allow numbers and letters
    var usernamePattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/); // only allow numbers, letters, and underscores

    // check if username or password field has been left empty
    if (user.username === null || user.password === null) {
      var errormessage = "Make sure everything is filled out correctly"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    }
    // check if email field has been left empty
    else if (user.email === null) {
      var errormessage = "Make sure everything is filled out correctly"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    }
    // check if username is longer than 3 characters
    // username should be 3 or more characters long
    else if (user.username.length < 3) {
      var errormessage = "Username must be at least 3 characters long"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    }
    // check if password is longer than 6 characters
    // password should be 6 or more characters long
    else if (user.password.length < 6) {
      var errormessage = "Password must be at least 6 characters long"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    }
    // check for special characters in username
    // username should only contain numbers, letters, and underscores
    else if (usernamePattern.test(user.username)) {
      var errormessage = "Usernames can only contain numbers, letters, and underscores"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    }
    // check for special characters in password
    // password should only contain numbers and letters
    else if (passwordPattern.test(user.password)) {
      var errormessage = "Passwords can only contain numbers and letters"
      $scope.showInvalidRegistrationCredentialsError(errormessage);
    } else {
      // sign up
      Auth.signUp(user).then(function(result) {
        // check for registration errors
        console.log(result);
        if (result.code == 202) { // username not availble
          var errormessage = "The username entered is not availble"
          $scope.showRegistrationError(errormessage);
        } else if (result.code == 203) { // email has is already in use
          var errormessage = "The email entered has already been registred"
          $scope.showRegistrationError(errormessage);
        } else if (result.code == 125) { // invalid email address
          var errormessage = "The email entered is not valid"
          $scope.showRegistrationError(errormessage);
        } else {
          // if no error and user was created
          $scope.clearUserFormInputs(); // clear user form
          $scope.closeSignUpModal(); // close sign up modal
          $ionicLoading.hide(); // hide loading
          $state.go('home', { // go to home
            clear: true
          });
        }

      });
    }

  }

  // display error message if log in credentials are invalid
  $scope.showLogInError = function(errormessage) {
    $ionicLoading.hide(); // hide loading
    $cordovaDialogs.alert(errormessage, 'Error', 'Try Again')
      .then(function() {
        $scope.clearUserFormInputs(); // clear user input
      });
  }

  // display error message if there is an error during sign up
  $scope.showRegistrationError = function(errormessage) {
    $ionicLoading.hide(); // hide loading
    $cordovaDialogs.alert(errormessage, 'Error', 'Try Again')
      .then(function() {
        $scope.clearUserFormInputs(); // clear user input
      });
  }

  // display error message if registration credentials are invalid
  $scope.showInvalidRegistrationCredentialsError = function(errormessage) {
    $ionicLoading.hide(); // hide loading
    $cordovaDialogs.alert(errormessage, 'Error', 'Try Again')
      .then(function() {
        $scope.clearUserFormInputs(); // clear user input
      });

  }

})
