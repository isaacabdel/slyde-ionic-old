// friends.js //
angular.module('slyde.friends', ['ionic'])

.controller('friends', function($scope, $ionicModal, $cordovaDialogs, $ionicListDelegate, $state, Friends) {

  // friends modal
  $ionicModal.fromTemplateUrl('app/friends/friends.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.friendsModal = modal;
  });
  $scope.openFriendsModal = function() {
    $scope.friendsModal.show();
  };
  $scope.closeFriendsModal = function() {
    $scope.friendsModal.hide();
  };

  // followers modal
  $ionicModal.fromTemplateUrl('app/friends/followers.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.followersModal = modal;
  });
  $scope.openFollowersModal = function() {
    $scope.followersModal.show();
    $scope.getFollowers();
  };
  $scope.closeFollowersModal = function() {
    $scope.followersModal.hide();
  };

  // following modal
  $ionicModal.fromTemplateUrl('app/friends/following.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.followingModal = modal;
  });
  $scope.openFollowingModal = function() {
    $scope.followingModal.show();
    $scope.getFollowing();
  };
  $scope.closeFollowingModal = function() {
    $scope.followingModal.hide();
  };


  /**
   * prompts current user to enter the username of a user they wish to follow
   */
  $scope.promptForUsername = function() {
    // prompt user to enter user to follow
    $cordovaDialogs.prompt('', 'Enter Username to Follow', ['Search', 'Cancel'], '')
      .then(function(result) {
        var input = result.input1; // input holds the username entered
        // check if current user entered own username
        if (input === (Parse.User.current()).getUsername()) { // current user can not follow itself
          // error message
          var errormessage = "You can not follow your self";
          // show error message
          $scope.showErrorMessage(errormessage);
        } else if (input) {
          // perform query in user class for the username entered
          var query = new Parse.Query(Parse.User);
          query.equalTo("username", input);
          query.find({
            success: function(user) { // returns user object
              // check if user exists
              if (user.length !== 0) {
                // if user exists
                $scope.addRelations(user);
              } else {
                // if user does not exists, show error message
                var errormessage = "No user by that name";
                $scope.showErrorMessage(errormessage);
              }
            }
          });
        } else {} // if no username was entered, do nothing
      }); // <-- end of prompt
  }

  /**
   * adds user relations
   * @param {Object} user: contains queried user
   */
  $scope.addRelations = function(user) {

    // add queried user to the current users following relation
    var currentUser = Parse.User.current();
    var currentUseRelation = currentUser.relation("following");
    currentUseRelation.add(user);
    currentUser.save();

    /*
     * cloud code function found in cloudcode/cloud/main.js folder
     * adds the current user to the queried users followers relation
     */
    Parse.Cloud.run('addRelations', {
      id: user[0].id // pass queried users object id
    }, {
      success: function(status) {
        console.log(status);
      },
      error: function(error) {
        console.log(error);
      }
    });
  }

  /**
   * displays error message
   * @param {String} errormessage: contains error message
   */
  $scope.showErrorMessage = function(errormessage) {
    $cordovaDialogs.alert('', errormessage, 'Try Again')
      .then(function() {

      });
  };


  $scope.unfollow = function(user) {
    console.log(user);
    $cordovaDialogs.confirm('', 'Are you sure you want to unfollow' + user.get('username'), ['Unfollow', 'Cancel'])
      .then(function(buttonIndex) {
        // no button = 0, 'OK' = 1, 'Cancel' = 2
        var btnIndex = buttonIndex;
        if (btnIndex === 1) {

          var currentUser = Parse.User.current();
          var currentUseRelation = currentUser.relation("following");
          currentUseRelation.remove(user);
          currentUser.save();

          // remove current user from the unfollowed user's followers list
          Parse.Cloud.run('removeRelations', {
            id: user.id
          }, {
            success: function(status) {
              console.log(status);
              $ionicListDelegate.closeOptionButtons();
            },
            error: function(error) {
              console.log(error);
              $ionicListDelegate.closeOptionButtons()
            }
          });
          //
        }
      });
  }

  /**
   * logs the current user out, and ends session
   */
  $scope.logOut = function() {
    Parse.User.logOut(); // log user out
    $scope.closeFriendsModal(); // close friends modal
    $state.go('welcome', { // go to welcome
      clear: true
    });
  }

  /**
   * gets the current users following
   */
  $scope.getFollowing = function() {
    Friends.getFollowing($scope.sessionUser).then(function(following) {
      // timeout
      setTimeout(function() {
        $scope.$apply(function() {
          $scope.following = following;
        });
      }, 0500);
    });
  }

  /**
   * gets the current users followers
   */
  $scope.getFollowers = function() {
    Friends.getFollowers($scope.sessionUser).then(function(followers) {
      // timeout
      setTimeout(function() {
        $scope.$apply(function() {
          $scope.followers = followers;
        });
      }, 0500);
    });
  }


})
