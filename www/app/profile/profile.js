angular.module('slyde.profile', ['ionic'])

.controller('profile', function($scope, $ionicModal, $ionicLoading, $cordovaDialogs, Slydes) {

  // friends modal
  $ionicModal.fromTemplateUrl('app/profile/profile.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.profileModal = modal;
  });
  $scope.openProfileModal = function(user) {
    $scope.profileModal.show();
    $scope.user = user;
    $scope.getSlydes(user);
  };
  $scope.closeProfileModal = function() {
    $scope.profileModal.hide();
    $scope.showSlydes = false;
  };

  $scope.getSlydes = function(user) {
    // hide slydes when loading new slydes
    $scope.showSlydes = false;
    // show loading
    $ionicLoading.show({
      template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner> <br> Loading...'
    });
    // get slydes
    Slydes.getSlydes(user).then(function(Slydes) {
      console.log(Slydes.length);
      $scope.slydes = Slydes;
      setTimeout(function() {
        $scope.$apply(function() {
          // show slydes
          $scope.showSlydes = true;
          // hide loading
          $ionicLoading.hide();
        });
      }, 0500);
    })
  }

  $scope.reportSlyde = function(slyde) {
    $cordovaDialogs.confirm('The will only flag the selected Slyde ', 'Flag Slyde?', ['Confirm', 'Cancel'])
      .then(function(buttonIndex) {
        // no button = 0, 'OK' = 1, 'Cancel' = 2
        var btnIndex = buttonIndex;

        if (btnIndex === 1) {
          var Report = Parse.Object.extend("Reports");
          var flag = new Report();
          flag.set('userReported', slyde.get('slydeBy'));
          flag.set('slydeReported', slyde);
          flag.set('reportedBy', Parse.User.current().getUsername());
          flag.save(null, {
            success: function() {
              $cordovaDialogs.alert('We will look into this promptly, and take the necessary precautions. Thank You.', 'Slyde was flagged!', 'OK')
                .then(function() {
                  // callback success
                });
            }
          });

        }
      });
  }

  $scope.reportSlyde = function(slyde) {
    $cordovaDialogs.confirm('The will only flag the selected Slyde ', 'Flag Slyde?', ['Confirm', 'Cancel'])
      .then(function(buttonIndex) {
        // no button = 0, 'OK' = 1, 'Cancel' = 2
        var btnIndex = buttonIndex;

        if (btnIndex === 1) {
          var Report = Parse.Object.extend("SlydeReports");
          var flag = new Report();
          flag.set('userReported', slyde.get('slydeBy'));
          flag.set('slydeReported', slyde);
          flag.set('reportedBy', Parse.User.current().getUsername());
          flag.save(null, {
            success: function() {
              $cordovaDialogs.alert('We will look in to this promptly, and take the necessary precautions. Thank You.', 'Slyde was flagged!', 'OK')
                .then(function() {
                  // callback success
                });
            }
          });

        }
      });
  }


  $scope.reportUser = function(user) {
    $cordovaDialogs.confirm('This will report ' + user.get('username')+ "'s post", 'Report ' + user.get('username'), ['Confirm', 'Cancel'])
      .then(function(buttonIndex) {
        // no button = 0, 'OK' = 1, 'Cancel' = 2
        var btnIndex = buttonIndex;

        if (btnIndex === 1) {
          var Report = Parse.Object.extend("UserReports");
          var flag = new Report();
          flag.set('userReported', user.get('username'));
          flag.set('postReported', user);
          flag.set('reportedBy', Parse.User.current().getUsername());
          flag.set('postUrl', user.get('post')._url);
          flag.save(null, {
            success: function() {
              $cordovaDialogs.alert('We will look into this promptly, and take the necessary precautions. Thank You.', 'User was flagged!', 'OK')
                .then(function() {
                  // callback success
                });
            }
          });
        }
      });
  }




})
