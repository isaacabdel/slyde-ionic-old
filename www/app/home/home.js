angular.module('slyde.home', ['ionic'])

.controller('home', function($scope, Friends) {

  $scope.getFollowing = function() {
    Friends.getFollowing($scope.sessionUser).then(function(following) {
      // timeout
      setTimeout(function() {
        $scope.$apply(function() {
          $scope.following = following;
        });
      }, 0500);
    });
  }
  $scope.getFollowing();

  $scope.refreshGetFollowing = function() {
    Friends.getFollowing($scope.sessionUser).then(function(following) {
      // timeout
      $scope.$broadcast('scroll.refreshComplete');
      setTimeout(function() {
        $scope.$apply(function() {
          $scope.following = following;
        });
      }, 0500);
    });
  }

})
