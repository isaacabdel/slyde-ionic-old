angular.module('slyde.draw', ['ionic'])

.controller('draw', function($scope, $ionicModal, $ionicPopover, $ionicLoading, $cordovaDialogs) {

  $ionicModal.fromTemplateUrl('app/draw/draw.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalDraw = modal;
  });

  $scope.openDrawModal = function(user) {
    $scope.modalDraw.show();
    $scope.draw(user);
  };
  $scope.closeDrawModal = function() {
    $scope.modalDraw.hide();
    $scope.modalDraw.remove(); // remove modalDraw to clear fabirc.js canvas
    // create a new instance of modalDraw
    $ionicModal.fromTemplateUrl('app/draw/draw.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalDraw = modal;
      $scope.showColorPaletteIcon = false; // hide color palette icon
    });

  };
  // alert user before closing modalDraw
  // ** this is called when the user clicks 'close' in draw.html **
  $scope.closeDrawModalAlert = function() {
    // alert user
    $cordovaDialogs.confirm('', 'Are you sure you want to exit?', ['Yes', 'Cancel'])
      .then(function(buttonIndex) {
        // no button = 0, 'Yes' = 1, 'Cancel' = 2
        var btnIndex = buttonIndex;
        if (btnIndex == 1) {
          // close modalDraw
          $scope.closeDrawModal();
        }

      });
  }


  var width = window.innerWidth;
  var height = width * (4 / 3)

  // initialize
  $scope.draw = function(user) {
      // initialize fabric.js
      var canvas = new fabric.Canvas('c');

      canvas.isDrawingMode = false;
      canvas.freeDrawingBrush.width = 6;

      canvas.setHeight(height);
      canvas.setWidth(width);

      canvas.width = width;
      canvas.height = height;

      fabric.Object.prototype.selectable = false; // prevent drawing objects to be selectable and draggable

      canvas.setBackgroundImage(user.get('post')._url, canvas.renderAll.bind(canvas), { // set background image
        height: height,
        width: width
      });


      // drawing mode
      $scope.drawingMode = function() {
        // check if fabric is in drawing mode
        if (canvas.isDrawingMode == true) {
          // if fabric is in drawing mode, exit drawing mode
          $scope.showColorPaletteIcon = false; // hind color palette icon
          canvas.isDrawingMode = false;
        } else {
          // if fabric is not in drawing mode, enter drawing mode
          $scope.showColorPaletteIcon = true; // show color palette icon
          canvas.isDrawingMode = true;
        }
      }

      // add text
      $scope.addText = function() {
        // exit drawing mode
        canvas.isDrawingMode = false;
        $scope.showColorPaletteIcon = false; // hide color palette icon

        // open dialog input
        $cordovaDialogs.prompt('', 'Add Text', ['Add', 'Cancel'], '')
          .then(function(result) {
            var input = result.input1;
            // no button = 0, 'Add' = 1, 'Cancel' = 2
            var btnIndex = result.buttonIndex;
            // check if btnIndex = 'Add' button index
            if (btnIndex == 1) {
              var t = new fabric.Text(input, {
                left: (width / 3),
                top: 100,
                fontFamily: 'Helvetica',
                fill: '#000', // color
                selectable: true, // draggbale

              });
              canvas.add(t); // add text to fabric.js canvas
            }

          });

      }

      // undo last obeject added, drawing or text
      $scope.undoLastObject = function() {
        var canvas_objects = canvas._objects;
        var last = canvas_objects[canvas_objects.length - 1];
        canvas.remove(last);
        canvas.renderAll();
      }

      // color options popover
      $ionicPopover.fromTemplateUrl('app/draw/colors.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
      });

      $scope.openColorsPopover = function($event) {
        $scope.popover.show($event);
      };
      $scope.closeColorsPopover = function() {
        $scope.popover.hide();
      };

      $scope.colors = [{
        code: "#ecf0f1"
      }, {
        code: "#95a5a6"
      }, {
        code: "#bdc3c7"
      }, {
        code: "#7f8c8d"
      }, {
        code: "#000000"
      }, {
        code: "#F1A9A0"
      }, {
        code: "#D2527F"
      }, {
        code: "#f1c40f"
      }, {
        code: "#f39c12"
      }, {
        code: "#e67e22"
      }, {
        code: "#d35400"
      }, {
        code: "#e74c3c"
      }, {
        code: "#c0392b"
      }, {
        code: "#6D4C41"
      }, {
        code: "#3E2723"
      }, {
        code: "#1abc9c"
      }, {
        code: "#16a085"
      }, {
        code: "#2ecc71"
      }, {
        code: "#27ae60"
      }, {
        code: "#3498db"
      }, {
        code: "#2980b9"
      }, {
        code: "#34495e"
      }, {
        code: "#2c3e50"
      }, {
        code: "#9b59b6"
      }, {
        code: "#8e44ad"
      }, ]


      // change brush color
      $scope.changeBrushColor = function(code) {
        canvas.freeDrawingBrush.color = code
        $scope.popover.hide(); // hide popover
      }

      // post slyde
      $scope.postSlyde = function() {
        // prompt user for confirmation
        $cordovaDialogs.confirm('', 'Would you like to post this slyde?', ['Yes', 'Cancel'])
          .then(function(buttonIndex) {
            // no button = 0, 'Yes' = 1, 'Cancel' = 2
            var btnIndex = buttonIndex;
            // check if btnIndex = 'Yes' button index
            if (btnIndex == 1) {
              // show uploading overlay
              $ionicLoading.show({
                template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner> <br> Uploading...'
              });
              //
              canvas.setBackgroundImage('', canvas.renderAll.bind(canvas)); // clear background image, to only save drawing


              // upload slyde
              var currentUser = Parse.User.current(); // current user
              var slydeBase64 = canvas.toDataURL();
              var slydeFile = new Parse.File("slyde.jpg", {
                base64: slydeBase64
              });

              slydeFile.save().then(function() {
                var Slyde = new Parse.Object("Slydes");
                Slyde.set("slyde", slydeFile);
                Slyde.set("slydeBy", currentUser.get('username'))
                Slyde.set("slydeTo", user.get('username'))
                Slyde.save(null, {
                  success: function() {
                    console.log("success");
                  }
                });
              }, function(error) {
                console.log("error")
              });

              $scope.closeDrawModal(); // close modalDraw
              $ionicLoading.hide(); // hide uploading overlay


            }

          });

      }



    } // * end of draw function *


})
